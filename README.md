# 3088 Pihat Project

Pihat acts as a UPS (uninterruptible power supply) for the raspberry pi in the event of the loss of supply power.

[Instructions on how to use it](instructions.md)
